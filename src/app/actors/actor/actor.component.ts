import { Component, OnInit } from '@angular/core';
import {MovieServiceService} from '../../movie-service.service';
import {ActivatedRoute} from '@angular/router';
@Component({
  selector: 'app-actor',
  templateUrl: './actor.component.html',
  styleUrls: ['./actor.component.sass'],
  providers: [MovieServiceService]
})
export class ActorComponent implements OnInit {
  actor:any;
  actorMovies:any;
  actorId:any;
  constructor(private ms: MovieServiceService, private route: ActivatedRoute) {

  	this.route.params.subscribe(params => {

  		this.actorId = +params['id'];

  		this.ms.getActor(this.actorId).subscribe(res => {
  			this.actor = res;
  			console.log("actor", res);

  			this.ms.getActorMovies(this.actorId).subscribe(res => {
  				console.log('actor mvoies', res)
  				this.actorMovies = res;
  			});

  		});

  	})

  }

  ngOnInit() {
  }

}
