import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import { MovieServiceService } from '../movie-service.service';
@Component({
  selector: 'app-actors',
  templateUrl: './actors.component.html',
  styleUrls: ['./actors.component.sass'],
  providers: [MovieServiceService]
})
export class ActorsComponent implements OnInit {

  page = 1;
  actors:any;

  constructor(private ms:MovieServiceService) {

  	this.ms.getPopularActors(this.page).subscribe(res => {
  		console.log("actor", res);
  		this.actors = res;
  	});

  }

  switchPage(number, where, category, totalPages){

    /* Top rated movies */

    //If user enters number instead of usign button
    if(where === 'number' && category === 'actors'){
      //If number is greater than total number of pages
      if(number > totalPages){
        this.page = 1;
        this.ms.getPopularActors(this.page).subscribe((res) => {
          this.actors = res;
        });
      } else {
        this.ms.getPopularActors(this.page = parseInt(number)).subscribe((res) => {
          this.actors = res;
        });
      }
    }

    //If user is using buttons
    if(number === '' && where === 'next' && category === 'actors'){
      this.ms.getPopularActors(this.page += 1).subscribe((res) => {
        this.actors = res;
      });
    } else if(number === '' && where === 'prev' && category === 'actors'){
      this.ms.getPopularActors(this.page -= 1).subscribe((res) => {
        this.actors = res;
      });
    }

  }

  ngOnInit() {
  }

}
