import {ModuleWithProviders} from '@angular/core';
import { RouterModule, Routes, CanActivate } from '@angular/router';
import {UserGuard} from './user/user.guard';
//Components
import { AppComponent } from './app.component';
import { SearchComponent } from './search/search.component';
import { MoviesComponent } from './movies/movies.component';
import { TvshowsComponent } from './tvshows/tvshows.component';
import { ActorsComponent } from './actors/actors.component';
import { ActorComponent } from './actors/actor/actor.component';
import { MovieComponent } from './movies/movie/movie.component';
import { TvshowComponent } from './tvshows/tvshow/tvshow.component';
import { MovieServiceService } from './movie-service.service';
import { GenresComponent } from './genres/genres.component';
import { GenreComponent } from './genres/genre/genre.component';
import { UserComponent } from './user/user.component';
import { EditProfileComponent } from './user/edit-profile/edit-profile.component';
import { CollectionsComponent } from './user/collections/collections.component';
//Routes
export const Routing:Routes = [

	{
		path: '',
		component: MoviesComponent
	},
	{
		path: 'movie/:id',
		component: MovieComponent
	},
	{
		path: 'search/:type/:value',
		component: SearchComponent
	},
	{
		path: 'genres',
		component: GenresComponent
	},
	{
		path: 'genres/:id',
		component: GenreComponent
	},
	{
		path: 'actors',
		component: ActorsComponent
	},
	{
		path: 'actor/:id',
		component: ActorComponent
	},
	{
		path: 'user/:id',
		component: UserComponent
	},
	{
		path: 'user/:id/edit',
		component: EditProfileComponent,
		canActivate: [UserGuard]
	},
	{
		path: 'user/:id/collections',
		component: CollectionsComponent,
		canActivate: [UserGuard]
	},
	{
		path: '**',
		component: MoviesComponent
	}

];

export const AppRoutes: ModuleWithProviders = RouterModule.forRoot(Routing);