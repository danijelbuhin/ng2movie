import {Injectable} from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs/Rx';
import { AngularFire } from 'angularfire2';
import { MovieServiceService } from '../../movie-service.service';
@Injectable()
export class UserGuard implements CanActivate {
	constructor(private af: AngularFire){

	}
	canActivate(route:ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean{
		let user:any;
		this.af.auth.subscribe(auth => {
			user = auth;
		});
		if(user != null){
			return true;
		}else {
			return false;
		}
	}
}