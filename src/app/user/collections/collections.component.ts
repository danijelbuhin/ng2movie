import { Component, OnInit } from '@angular/core';
import { AngularFire, FirebaseListObservable } from 'angularfire2';
import {ActivatedRoute} from '@angular/router';
import { MovieServiceService } from '../../movie-service.service';
@Component({
  selector: 'app-collections',
  templateUrl: './collections.component.html',
  styleUrls: ['./collections.component.sass']
})
export class CollectionsComponent implements OnInit {
  currentUser:any;
  collections:FirebaseListObservable<any>;
  movies: FirebaseListObservable<any>;


  constructor(private af: AngularFire, private route: ActivatedRoute, private ms: MovieServiceService) {

  	this.af.auth.subscribe(auth => {
  		this.currentUser = auth;
  	});

	this.collections = this.af.database.list(`/users/${this.currentUser.uid}/collections`);
	this.collections.subscribe(snap => {

		this.movies = snap;
		
	})
	

  }

  removeFromCollection(movieId){
  	this.af.database.list(`/users/${this.currentUser.uid}/collections`).remove(movieId.toString());
  }

  ngOnInit() {
  }

}
