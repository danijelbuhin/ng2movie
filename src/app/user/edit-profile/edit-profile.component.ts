import { Component, OnInit } from '@angular/core';
import { AngularFire, FirebaseListObservable } from 'angularfire2';
import {ActivatedRoute, Router} from '@angular/router';
import { MovieServiceService } from '../../movie-service.service';
@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.sass']
})
export class EditProfileComponent implements OnInit {
  uid:any;
  auth:any;
  currentUser:any;
  constructor(private af: AngularFire, private ms: MovieServiceService, private route: ActivatedRoute, private router: Router) {
  	this.route.params.subscribe(params => {

  		this.uid = params['id'];

  		this.af.auth.subscribe(auth => {
        this.auth = auth;
  		});

      this.af.database.list(`/users`, {
        query: {
            orderByKey: true,
            equalTo: this.uid,
            limitToFirst: 1
          }
      }).subscribe(snap => {
        this.currentUser = snap;
        console.log(this.currentUser);
      })
      
  	}); 
  }
  updateUserProfile(name, bio, fav_movie, age){
    this.af.database.list(`/users`).update(this.uid, {
      bio: bio || this.currentUser[0].bio,
      fav_movie: fav_movie || this.currentUser[0].fav_movie,
      age: age || this.currentUser[0].age
    });
  }
  ngOnInit() {
  }

}
