import { Component, OnInit } from '@angular/core';
import { AngularFire, FirebaseListObservable } from 'angularfire2';
import {ActivatedRoute} from '@angular/router';
import { MovieServiceService } from '../movie-service.service';
@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.sass']
})
export class UserComponent implements OnInit {
  uid:any;
  currentUser:any;
  userData: FirebaseListObservable<any>;
  collMovies: FirebaseListObservable<any>;
  constructor(private af: AngularFire, private ms: MovieServiceService, private route: ActivatedRoute) {
  	this.route.params.subscribe(params => {

  		this.uid = params['id'];


  		this.af.auth.subscribe(auth => {
  			this.currentUser = auth;
  		});

		//Get user data
		this.userData = this.af.database.list(`/users`, {
			query: {
	          orderByKey: true,
	          equalTo: this.uid,
	          limitToFirst: 1
        	}
		});

    this.af.database.list(`/users/${this.uid}/collections`).subscribe(snap => {
      this.collMovies = snap;
    })
		

  	});
  	
  }

  ngOnInit() {
  }

}
