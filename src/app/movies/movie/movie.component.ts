import { Component, OnInit } from '@angular/core';
import {MovieServiceService} from '../../movie-service.service';
import {ActivatedRoute} from '@angular/router';
import { AngularFire, FirebaseListObservable } from 'angularfire2';
@Component({
  selector: 'app-movie',
  templateUrl: './movie.component.html',
  styleUrls: ['./movie.component.sass'],
  providers: [MovieServiceService]
})
export class MovieComponent implements OnInit {
	//Current Movie Id
	currentMovie:any;

	//Current movie object
	movie:Object;

	//Current movie similar movies
	similarMovies:any;

	//Current user
	uid:any;
	currentUser:any;
	customCollections:FirebaseListObservable<any>;
	inColl:boolean;
	constructor(private ms: MovieServiceService, private route: ActivatedRoute, private af: AngularFire) {
		//Get Current Movie Id
		this.route.params.subscribe(params => {
			this.currentMovie = +params['id'];
			this.inColl = false;
			//Get current movie object
			this.ms.getMovie(this.currentMovie).subscribe((res) => {
				this.movie = res;
			});

			//Get current movie similar movies
			this.ms.getSimilar(this.currentMovie).subscribe((res) => {
				this.similarMovies = res;
			});

		});

		//User details
		this.route.params.subscribe(params => {
			this.uid = params['id'];
		});

		this.af.auth.subscribe(auth => {
			this.currentUser = auth;
			if(auth){
				this.customCollections = this.af.database.list(`/users/${this.currentUser.uid}/customCollections`);
			}
		});

	}

	addToCollection(name:string, img:string, id:number){
		this.inColl = true;
		this.af.database.list(`/users/${this.currentUser.uid}/collections`).update(id.toString(), {
			name: name,
			img: img,
			id: id,
			isPublic: true
		});
		
	}
	/*isInCollection(movie){
		this.userCollections.subscribe(snap => {
			for (var i = 0; i < snap.length; i++) {
				if(this.currentMovie === snap[i].id){
					this.inColl = true;
					break;
				} else {
					this.inColl = false;
				}
			}
		})
	}*/
	ngOnInit() {

	}


}
