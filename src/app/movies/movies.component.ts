import { Component, OnInit, EventEmitter } from '@angular/core';
import {MovieServiceService} from '../movie-service.service';
import { AngularFire, FirebaseListObservable } from 'angularfire2';
import {ActivatedRoute} from '@angular/router';
@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.sass']
})
export class MoviesComponent implements OnInit {
	//Top Rated Movies
	topRatedMovies:any;
	trmPage = 1;

	//Popular Movies
	popularMovies:any;
	pmPage = 1;

	

	constructor(private ms: MovieServiceService, private af: AngularFire, private route: ActivatedRoute) {


		//Get top rated movies
		this.ms.homeTopRated(this.trmPage).subscribe((res) => {
			this.topRatedMovies = res;
		});

		//Get popular movies
		this.ms.homePopular(this.pmPage).subscribe((res) => {
			this.popularMovies = res;
		});


	}
	switchPage(number, where, category, totalPages){

		/* Top rated movies */

		//If user enters number instead of usign button
		if(where === 'number' && category === 'topRated'){
			//If passed number is greater than total pages number
			if(number > totalPages){
				this.trmPage = 1;
				this.ms.homeTopRated(this.trmPage).subscribe(res => {
					this.topRatedMovies = res;
				});
			} else {
				this.ms.homeTopRated(this.trmPage = parseInt(number)).subscribe((res) => {
					this.topRatedMovies = res;
				});
			}
			
		}

		//If user is using buttons
		if(number === '' && where === 'next' && category === 'topRated'){
			this.ms.homeTopRated(this.trmPage += 1).subscribe((res) => {
				this.topRatedMovies = res;
			});
		} else if(number === '' && where === 'prev' && category === 'topRated'){
			this.ms.homeTopRated(this.trmPage -= 1).subscribe((res) => {
				this.topRatedMovies = res;
			});
		}



		/* Popular movies */

		//If user enters and number instead of usign button
		if(where === 'number' && category === 'popular'){
			//If passed number is greater than total pages number
			if(number > totalPages){
				this.pmPage = 1;
				this.ms.homePopular(this.pmPage).subscribe(res => {
					this.popularMovies = res;
				});
			} else {
				this.ms.homePopular(this.pmPage = parseInt(number)).subscribe((res) => {
					this.popularMovies = res;
				});
			}
			
		}

		//If user is using buttons
		if(where === 'next' && category === 'popular'){
			this.ms.homePopular(this.pmPage += 1).subscribe((res) => {
				this.popularMovies = res;
			});
		} else if(where === 'prev' && category === 'popular'){
			this.ms.homePopular(this.pmPage -= 1).subscribe((res) => {
				this.popularMovies = res;
			});
		}

	}

	ngOnInit() {

	}



}
