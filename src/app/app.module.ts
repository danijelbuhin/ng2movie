import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule, JsonpModule } from '@angular/http';
import { AngularFireModule } from 'angularfire2';
import { AngularFire } from 'angularfire2';
import { AppComponent } from './app.component';
import { SearchComponent } from './search/search.component';
import { MoviesComponent } from './movies/movies.component';
import { TvshowsComponent } from './tvshows/tvshows.component';
import { ActorsComponent } from './actors/actors.component';
import { ActorComponent } from './actors/actor/actor.component';
import { MovieComponent } from './movies/movie/movie.component';
import { TvshowComponent } from './tvshows/tvshow/tvshow.component';
import { MovieServiceService } from './movie-service.service';
import {AppRoutes} from './app.routing';
import { GenresComponent } from './genres/genres.component';
import { GenreComponent } from './genres/genre/genre.component';
import { UserComponent } from './user/user.component';
import { EditProfileComponent } from './user/edit-profile/edit-profile.component';
import { CollectionsComponent } from './user/collections/collections.component';


import {UserGuard} from './user/user.guard';
export const firebaseConfig = {
    apiKey: "AIzaSyBMVZS0DVoc_m1idBs_nJi_DJxWdVYMOOQ",
    authDomain: "ngmovies-2c8be.firebaseapp.com",
    databaseURL: "https://ngmovies-2c8be.firebaseio.com",
    storageBucket: "ngmovies-2c8be.appspot.com",
    messagingSenderId: "266162057541"
};

@NgModule({
  declarations: [
    AppComponent,
    SearchComponent,
    MoviesComponent,
    TvshowsComponent,
    ActorsComponent,
    ActorComponent,
    MovieComponent,
    TvshowComponent,
    GenresComponent,
    GenreComponent,
    UserComponent,
    EditProfileComponent,
    CollectionsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    JsonpModule,
    AppRoutes,
    AngularFireModule.initializeApp(firebaseConfig)
  ],
  providers: [MovieServiceService, AngularFire, UserGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
