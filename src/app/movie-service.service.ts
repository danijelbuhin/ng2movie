import { Injectable } from '@angular/core';
import {Http} from '@angular/http';
import { AngularFire, FirebaseListObservable, AuthMethods, AuthProviders } from 'angularfire2';
import 'rxjs/Rx';
@Injectable()
export class MovieServiceService {
	private key: any = '75061d087e91e83a6cdc87d86073c09c';
	
	constructor(private http: Http, private af: AngularFire) {
		
	}

	//Home top rated movies
	homeTopRated(page) {
		return this.http.get(`https://api.themoviedb.org/3/movie/top_rated?api_key=75061d087e91e83a6cdc87d86073c09c&language=en-US&page=${page}`)
		.map(res => {
			return res.json();
		})
	}

	//Home popular movies
	homePopular(page) {
		return this.http.get(`https://api.themoviedb.org/3/movie/popular?api_key=75061d087e91e83a6cdc87d86073c09c&language=en-US&page=${page}`)
		.map(res => {
			return res.json();
		})
	}

	//Movie
	getMovie(movie){
		return this.http.get(`https://api.themoviedb.org/3/movie/${movie}?api_key=75061d087e91e83a6cdc87d86073c09c&language=en-US`)
		.map(res => {
			return res.json();
		})
	}

	//Get similar movies
	getSimilar(movieId){
		return this.http.get(`https://api.themoviedb.org/3/movie/${movieId}/similar?api_key=75061d087e91e83a6cdc87d86073c09c&language=en-US&page=1`)
		.map(res => {
			return res.json();
		})
	}


	//Search
	search(type, value, page){

		if(type === 'movie'){
			return this.http.get(`https://api.themoviedb.org/3/search/movie?api_key=75061d087e91e83a6cdc87d86073c09c&language=en-US&query=${value}&page=${page}`)
			.map(res => {
				return res.json();
			});
		} else if(type === 'actor'){
			return this.http.get(`https://api.themoviedb.org/3/search/person?api_key=75061d087e91e83a6cdc87d86073c09c&language=en-US&query=${value}&page=${page}&include_adult=false`)
			.map(res => {
				return res.json();
			});
		} else {
			return this.http.get(`https://api.themoviedb.org/3/search/movie?api_key=75061d087e91e83a6cdc87d86073c09c&language=en-US&query=${value}&page=${page}`)
			.map(res => {
				return res.json();
			});
		}

		
	}

	//Genres 
	 //Genre name 
	 getGenreName(){
	 	return this.http.get(`https://api.themoviedb.org/3/genre/movie/list?api_key=75061d087e91e83a6cdc87d86073c09c&language=en-US`)
	 	.map(res => res.json())
	 }

	 //Movies by genre 
	 getMoviesGenre(id, sort){
	 	return this.http.get(`https://api.themoviedb.org/3/genre/${id}/movies?api_key=75061d087e91e83a6cdc87d86073c09c&language=en-US&sort_by=${sort}`)
	 	.map(res => res.json())
	 }


	//Actors
	getPopularActors(page){
		return this.http.get(`https://api.themoviedb.org/3/person/popular?api_key=75061d087e91e83a6cdc87d86073c09c&language=en-US&page=${page}`)
		.map(res => res.json())
	}

	getActor(id){
		return this.http.get(`https://api.themoviedb.org/3/person/${id}?api_key=75061d087e91e83a6cdc87d86073c09c&language=en-US`)
		.map(res => res.json())
	}
	getActorMovies(id){
		return this.http.get(`https://api.themoviedb.org/3/person/${id}/movie_credits?api_key=75061d087e91e83a6cdc87d86073c09c&language=en-US`)
		.map(res => res.json())
	}

	//User profile
	isAuth(){
		this.af.auth.subscribe(auth => {
			if(auth != null){
				return true;
			} else {
				return false;
			}
		})
	}


}
