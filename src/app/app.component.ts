import { Component } from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import { MovieServiceService } from './movie-service.service';
import { AngularFire, FirebaseListObservable, AuthMethods, AuthProviders } from 'angularfire2';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  res:any;
  search_type = [
  	{type: 'movie'},
  	{type: 'actor'}
  ];
  search_value:string;
  user: any;
  userData:FirebaseListObservable<any>;
  users:FirebaseListObservable<any>;

  constructor(private ms: MovieServiceService, private router: Router, private route: ActivatedRoute, private af: AngularFire){

  	ms.homePopular('1').subscribe(res => {
  		this.res = res;
  	});

    this.af.auth.subscribe(auth => {
      this.user = auth;
      console.log(auth);

      if(this.user){

        let settings = {
          name: this.user.auth.displayName,
          uid: this.user.uid,
          status: 'Active',
          profilePhoto: this.user.auth.photoURL,
          email: this.user.auth.email
        };

        this.af.database.list('/users').update(auth.uid, settings);


        //Retreive data 
        this.userData = this.af.database.list(`/users/${this.user.uid}`);
    }     

    });

    this.users = this.af.database.list(`/users`);
  }
  register(method){

    if(method === 'google'){

      this.af.auth.login({
        provider: AuthProviders.Google,
        method: AuthMethods.Popup
      })

    } else if(method === 'facebook'){
      this.af.auth.login({
        provider: AuthProviders.Facebook,
        method: AuthMethods.Popup
      })
    }
  }

  logOut(){
    this.af.database.list(`/users`).update(this.user.uid, {
      status: 'Offline'
    });
    this.af.auth.logout();
  }
}


