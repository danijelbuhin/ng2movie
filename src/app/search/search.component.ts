import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import { MovieServiceService } from '../movie-service.service';
@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.sass'],
  providers: [MovieServiceService]
})
export class SearchComponent implements OnInit {
  result: any;
  type:any;
  page: any;
  results:any;
  constructor(private ms: MovieServiceService, private router: Router, private route: ActivatedRoute) {

  	this.route.params.subscribe(params => {
      this.type = params['type'];
  		this.result = params['value'];
      console.log(this.type, this.result);
      
  		this.ms.search(this.type, this.result, 1).subscribe(res => {
  			console.log('search', res);
  			this.results = res;
  		});
  	});
  	


  }

  switchPage(number, where, totalPages){

		/* Search results */

		//If user enters number instead of usign button
		if(where === 'number'){
			
      //If the number is greater than total number of pages
      if(number > totalPages){
        this.page = 1;
        this.ms.search(this.result.value, this.page).subscribe((res) => {
          this.results = res;
        });
      } else {
        this.ms.search(this.result.value, this.page = parseInt(number)).subscribe((res) => {
          this.results = res;
        });
      }
			

		}

		//If user is using buttons
		if(number === '' && where === 'next'){
			this.ms.search(this.result.value, this.page += 1).subscribe((res) => {

				this.results = res;

			});
		} else if(number === '' && where === 'prev'){
			this.ms.search(this.result.value, this.page -= 1).subscribe((res) => {
				
        this.results = res;

			});
		}

	}

  ngOnInit() {
  }

}
