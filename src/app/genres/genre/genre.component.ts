import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import { MovieServiceService } from '../../movie-service.service';
@Component({
  selector: 'app-genre',
  templateUrl: './genre.component.html',
  styleUrls: ['./genre.component.sass'],
  providers: [MovieServiceService]
})
export class GenreComponent implements OnInit {
  genre:any;
  genreMovies:any;
  genreId:any;
  page = 1;
  constructor(private ms: MovieServiceService, private route: ActivatedRoute, private router: Router) {

    //Get genre informations
  	this.route.params.subscribe(params => {
  		this.genreId = +params['id'];
  		this.ms.getGenreName().subscribe(res => {


  			for(let i = 0; i < res.genres.length; i++){

  				if(this.genreId === res.genres[i].id){
            //Get genre name
            this.genre = res.genres[i].name;

            //Get genre movies
  					this.ms.getMoviesGenre(this.genreId, 'created_at.asc').subscribe(res => {
  						console.log('movies', res);
  						this.genreMovies = res;
  					})
  				}


  			}/*for loop*/


  		});
  	});



  }

  

  ngOnInit() {
  }

}
