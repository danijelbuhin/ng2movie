import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import { MovieServiceService } from '../movie-service.service';
@Component({
  selector: 'app-genres',
  templateUrl: './genres.component.html',
  styleUrls: ['./genres.component.sass'],
  providers: [MovieServiceService]
})
export class GenresComponent implements OnInit {
  genres:any;
  genreMovies:any;
  constructor(private ms: MovieServiceService, private route: ActivatedRoute, private router: Router) {

  	//Get genre name 
  	this.route.params.subscribe(params => {
  		this.ms.getGenreName().subscribe(res => {
  			this.genres = res.genres;
        console.log(res);
  		});
  	});


  }

  ngOnInit() {
  }

}
