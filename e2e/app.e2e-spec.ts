import { Ng2moviePage } from './app.po';

describe('ng2movie App', function() {
  let page: Ng2moviePage;

  beforeEach(() => {
    page = new Ng2moviePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
